#! /usr/bin/env python3
'''GUI for renaming files containing URL-quoted characters in their names.'''

import urllib.request
import os
import os.path
import re
from tkinter import tix

class Application(tix.Frame):
    '''The GUI'''

    bad_char_regex = re.compile(r'[\\/:*?"<>|]')

    def __init__(self, master=None):
        tix.Frame.__init__(self, master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        '''Construct main window'''
        self.top_frame = tix.Frame(self, width=760, height=240)
        # disable propagation so the Text below won't resize it
        self.top_frame.pack_propagate(False)
        self.top_frame.pack(side="top")

        self.bottom_frame = tix.Frame(self)
        self.bottom_frame.pack(side="bottom")

        self.dir_select = tix.DirSelectBox(self.top_frame)
        self.dir_select.pack(side="left")

        self.text = tix.Text(self.top_frame)
        self.text.pack(side="right")
        self.text.insert('1.0', 'Please choose a folder and run')

        self.run = tix.Button(self.bottom_frame, text="run", command=self.operate_on_folder)
        self.run.pack(side="left")

        self.quit = tix.Button(self.bottom_frame, text="quit", command=self.master.destroy)
        self.quit.pack(side="right")

    def append_message(self, message):
        '''Append message to the output text area'''
        self.text.insert(tix.END, '\n'+message)

    def clear_output(self):
        '''Clear the output text area'''
        self.text.delete('1.0', tix.END)

    def operate_on_folder(self):
        '''Look for and rename offending files in the currently selected folder'''
        # the selected folder
        folder = self.dir_select.dircbx.cget('text')

        self.clear_output()

        if not os.path.isdir(folder):
            self.append_message('{}'.format(folder))
            return

        self.text.insert('1.0', 'Operating on {}'.format(folder))

        # get folder contents
        contents = os.listdir(folder)

        found = renamed = 0
        for file in contents:
            # path to file
            file_path = os.path.join(folder, file)

            # skip folders, etc
            if not os.path.isfile(file_path):
                continue

            destination_base = urllib.request.unquote(file)
            if destination_base == file:
                # nothing to do
                continue

            found += 1

            # remove bad chars
            destination_base = self.bad_char_regex.sub('_', destination_base)

            # path to destination
            destination = os.path.join(folder, destination_base)

            if os.path.exists(destination):
                self.append_message('Destination exists; skipping:\n'
                                    '  {} -> {}'.format(file, destination_base))
                continue
            else:
                os.rename(file_path, destination)
                self.append_message('{} -> {}'.format(file, destination_base))
                renamed += 1

        if found > 0:
            self.append_message('Renamed {} files\nAll done'.format(renamed))
        else:
            self.append_message('No escaped files found')

if __name__ == '__main__':
    root = tix.Tk()
    app = Application(master=root)
    app.mainloop()
