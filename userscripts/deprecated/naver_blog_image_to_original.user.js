// ==UserScript==
// @name          Naver blog - image to original
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirects naver blog images to the original
// @include       https://postfiles*.naver.net/*
// @include       http://postfiles*.naver.net/*
// @version       1.1
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  // get rid of image so it won't load twice
  var images = document.getElementsByTagName('img');
  if (images.length > 0) {
    images[0].parentNode.removeChild(images[0]);
  }

  // redirect
  var url = window.location.href,
      // also remove arguments used to downscale or otherwise modify the image
      new_url = url.replace(/postfiles\d+/, 'blogfiles').replace(/\?[^?]+$/, '');

  // make sure we don't loop infinitely
  if (new_url !== url) {
    // redirect
    window.location.replace(new_url);
  }

}());
