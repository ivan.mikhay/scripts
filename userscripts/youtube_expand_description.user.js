// ==UserScript==
// @name          Youtube - expand description
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Automatically expand a youtube video's description
// @include       https://www.youtube.com/*
// @include       http://www.youtube.com/*
// @version       2.0
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  // don't run on frames
  // http://stackoverflow.com/a/4190594
  if (window.top != window.self) { return; }

  var last_url,
    selectors = {
      'old_desc': '#action-panel-details',
      'new_desc': 'ytd-expander.description',
      'new_unhide': [
        'ytd-expander>.ytd-video-secondary-info-renderer>div[hidden]',
        'ytd-expander paper-button[id="less"]',
      ],
      'new_hide': [
        'ytd-expander paper-button[id="more"]'
      ]
    };

  function check_old_youtube() {
    var elem = document.querySelector(selectors.old_desc);

    if (elem && elem.classList.contains('yt-uix-expander-collapsed')) {
      // expand description
      elem.classList.remove('yt-uix-expander-collapsed');

      // save url so we don't run again until the user navigates away
      last_url = window.location.href;
    }
  }

  function check_new_youtube() {
    var i, elem = document.querySelector(selectors.new_desc);

    if (elem && elem.hasAttribute('collapsed')) {
      // expand description
      elem.removeAttribute('collapsed');

      for (i=0; i < selectors.new_unhide; i++) {
        elem = document.querySelector(selectors.new_unhide[i]);
        if (elem) {
          elem.removeAttribute('hidden');
        }
      }

      for (i=0; i < selectors.new_hide; i++) {
        elem = document.querySelector(selectors.new_hide[i]);
        if (elem) {
          elem.removeAttribute('hidden');
        }
      }

      // save url so we don't run again until the user navigates away
      last_url = window.location.href;
    }
  }

  function check_description() {
    // don't run again on the same page until the user navigates away
    if (window.location.href === last_url) {
      return;
    }
    else if (window.location.pathname.search('watch') < 0) {
      // save url in case the user goes back to the last seen video
      last_url = window.location.href;
      return;
    }

    if (document.querySelector(selectors.new_desc)) {
      check_new_youtube();
    }
    else {
      check_old_youtube();
    }
  }

  function initial_setup() {
    var observer = new MutationObserver(check_description);
    observer.observe(document, { childList: true, subtree: true });

    check_description();
  }

  document.addEventListener('DOMContentLoaded', initial_setup);
}());
