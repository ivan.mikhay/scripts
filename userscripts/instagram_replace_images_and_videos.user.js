// ==UserScript==
// @name          Instagram - replace images and videos
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Makes instagram images and videos into proper img and video tags
// @include       http*://*.instagram.com/*
// @include       http*://instagram.com/*
// @version       4.9
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var autoload_videos = true,
    debug = false,
    new_elements_observer = new MutationObserver(look_for_new_divs),
    new_elements_observer_init = {childList: true, subtree: true};

  function blur_arrows() {
    /* blur arrow element, if it is active, since it causes a dotted outline
     * across the center of the screen */
    if (document.activeElement.classList.contains('coreSpriteLeftPaginationArrow') ||
        document.activeElement.classList.contains('coreSpriteRightPaginationArrow')) {
      if (debug) {
        console.log('active element is '+
                    document.activeElement.className+'; blurring');
      }
      document.activeElement.blur();
    }
  }

  function expose_video(video) {
    var div = video.parentNode;

    video.controls = 'controls';
    video.preload = 'metadata';
    if (autoload_videos) {
      video.preload = 'auto';
    }
    video.dataset.handled = 'true';

    // reinsert as the first element of the parent div, so we can use it to
    // hide its siblings
    div.insertBefore(video, div.firstChild);

    // stop propagation of clicks, so instagram won't think it is receiving a
    // click from the clickshield and pause/play the video when it shouldn't
    video.addEventListener('click', function(event) {
      event.stopPropagation();
    });

    /* try to locate video's clickshields by going up its ancestors looking for
     * the role="button" elements. At this point in time there are two such
     * shields: one that is a sibling to one of the video's ancestors
     * (div[aria-label="Control"]) and one that is the child of another sibling
     * of that same ancestor (div>span[aria-label="Play"]). */
    while (div && div.tagName == 'DIV' && div.parentNode) {
      if (div.parentNode.querySelector('[role="button"]')) {
        div.classList.add('video_clickshield_sibling');
        break;
      }
      div = div.parentNode;
    }

    // blur arrow elements, when applicable
    blur_arrows();
  }

  function check_videos() {
    /* expose videos */
    var i, video,
      videos = document.querySelectorAll('video:not([data-handled="true"])');

    if (videos.length === 0) { return; }

    //if (debug) { console.log('found '+videos.length+' new videos'); }

    for (i=0; i < videos.length; i++) {
      video = videos[i];

      // nothing to do
      if (video.dataset.handled == 'true') { continue; }

      if (debug) { console.log('found new video'); }

      expose_video(video);
    }
  }

  function is_element_on_screen(elem) {
    // based on http://stackoverflow.com/a/7557433
    if (elem.getBoundingClientRect === undefined) { return false; }

    var rect = elem.getBoundingClientRect();
    return (
      rect.right >= 0 &&
      rect.bottom >= 0 &&
      rect.left <= (window.innerWidth || document.documentelem.clientWidth) &&
      rect.top <= (window.innerHeight || document.documentelem.clientHeight)
    );
  }

  function block_image_srcset(mutations) {
    /* Block the srcset attribute, so the browser will use src, which should
     * contain the highest resolution available */

    var image = mutations[0].target;

    /* note that we check for a 1080 width size in the srcset to avoid
     * operating on thumbs, and also that we check that the src in present in
     * srcset (which should always be the case) just to make sure we don't
     * mangle all images if instagram changes the way their js handles these
     * attributes in the future */
    if (!image || !image.src || !image.srcset ||
        (image.srcset.indexOf(' 1080w') < 0) ||
        (image.srcset.indexOf(image.src) < 0)) {
      return;
    }

    image.srcset = '';
    if (debug) { console.log('blocked srcset for image'); }
  }

  function hide_image_clickshields() {
    /* Go through all unhandled images and look for their click shields. */
    var i, image, observer,
        images = document.querySelectorAll('img:not([data-handled="true"])');

    for (i=0; i<images.length; i++) {
      image = images[i];

      // try to skip images that are not photos (e.g. avatars will be inside
      // <a> tags)
      if (!(image.parentNode && image.parentNode.tagName === 'DIV')) {
        continue;
      }

      /* If the image is off screen and the parent has other children, that
       * means it's being preloaded. The page will actually insert this image
       * as a new element once the post becomes visible, so we don't need to do
       * anything here (and if we try to, we might wrongfully hide
       * non-clickshield divs) */
      if (!is_element_on_screen(image) &&
          image.parentNode.childNodes.length > 1) {
        continue;
      }

      // if this is an image cover for a video, we don't have to do anything
      if (image.parentNode.querySelector('video')) {
        image.dataset.handled = 'true';
        return;
      }

      // if the image has a div sibling, that's the clickshield, otherwise it
      // will be its parent's sibling
      if (!image.parentNode.querySelector('div')) {
        image.parentNode.classList.add('clickshield_sibling');
      }

      // observe image attributes to block srcset
      observer = new MutationObserver(block_image_srcset);
      observer.observe(image, {'attributes': true});

      // mark image so we don't process it again in the future
      image.dataset.handled = 'true';
    }
  }

  function mark_image_count() {
    /* If the right chevron is available, use it to locate and mark the image
     * count for multi-image posts */

    var div,
        chevron = document.querySelector('.coreSpriteRightChevron'+
                                         ':not([data-handled="true"])');

    if (!chevron) { return; }
    // the image count is currently the next element sibling of the chevron's
    // grandparent
    div = chevron.parentNode.parentNode;
    if (div && div.nextElementSibling &&
        div.nextElementSibling.tagName == 'DIV') {

      div.nextElementSibling.classList.add('image_count');

      // mark chevron so we don't process it again in the future
      chevron.dataset.handled = 'true';
    }
  }

  function look_for_new_divs() {
    /* Go through all images and videos looking for new ones after a mutation */

    // hide click shield divs to expose the images underneath
    hide_image_clickshields();

    // mark the image count div in multi-image posts so we reposition it
    mark_image_count();

    // expose videos when applicable
    check_videos();
  }

  function setup_css() {
    /* insert new sheet for our custom rules */
    var sheet, elem = document.createElement('style');

    document.head.appendChild(elem);

    // note that we can only access the sheet after appending the element to
    // <head>
    sheet = elem.sheet;

    /* hide clickshields */
    sheet.insertRule(
      '.clickshield_sibling+div, '+
      '.video_clickshield_sibling+div, '+
      '.video_clickshield_sibling+div+[role="button"], '+
      'img[data-handled="true"]+div, '+
      'video[data-handled="true"]~div, '+
      'video[data-handled="true"]~canvas, '+
      'video[data-handled="true"]~img { display: none; }',
      sheet.cssRules.length);

    /* move image count to the top so it isn't obscured by the video controls */
    sheet.insertRule('div.image_count { top: 15px; bottom: initial; }',
                     sheet.cssRules.length);

    if (debug) { console.log('custom css inserted'); }
  }

  function setup() {
    if (debug) { console.log('setting up'); }

    /* setup CSS to unblock images */
    setup_css();

    /* watch whole DOM for new elements being added */
    new_elements_observer.observe(document, new_elements_observer_init);

    if (debug) { console.log('done'); }
  }

  document.addEventListener('DOMContentLoaded', setup);

}());
