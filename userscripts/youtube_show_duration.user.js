// ==UserScript==
// @name          Youtube - show duration
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Show video duration under the video's title
// @include       https://www.youtube.com/*
// @include       http://www.youtube.com/*
// @version       1.1
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  // don't run on frames
  // http://stackoverflow.com/a/4190594
  if (window.top != window.self) { return; }

  var last_href;

  function find_duration() {
    var content_elem, items, duration = null;

    content_elem = document.getElementById('content');
    items = content_elem.getElementsByTagName('meta');
    for (var i=0; i < items.length; i++) {
      if (items[i].getAttribute('itemprop') == 'duration') {
        duration = items[i].getAttribute('content');
        break;
      }
    }

    return duration;
  }

  function padded_str(number) {
    // returns number padded with a zero if necessary
    if (number < 10) {
      return '0'+number;
    }
    return number.toString();
  }

  function show_duration(duration) {
    if (duration === null) { return; }
    var match, text, headline,
      hours = 0, minutes, seconds,
      time = '', span;

    match = /PT(\d+)M(\d+)S/.exec(duration);
    if (match === null) { return; }

    // format time
    minutes = parseInt(match[1]);
    seconds = parseInt(match[2]);
    if (minutes > 60) {
      hours = parseInt(minutes/60);
      minutes = minutes % 60;
      // only show hours if greater than zero
      time = hours+':';
    }
    time += padded_str(minutes)+':'+padded_str(seconds);

    // add formatted duration under the video's title
    text = document.createTextNode(time);
    span = document.createElement('SPAN');
    span.id = 'duration_span';
    span.appendChild(text);

    headline = document.getElementById('watch7-headline');
    headline.appendChild(span);
  }

  function setup() {
    if (window.location.href === last_href) { return; }

    if (window.location.pathname.search('watch') < 0) {
      // stop watching this page
      last_href = window.location.href;
      return;
    }

    /* keep watching new watch page for mutations until the new header gets
     * created */
    if (document.getElementById('watch7-headline') &&
        !document.getElementById('duration_span')) {
      // stop watching this page
      last_href = window.location.href;

      show_duration(find_duration());
    }
  }

  function initial_setup() {
    var observer = new MutationObserver(setup);
    observer.observe(document, { childList: true, subtree: true });

    setup();
  }

  document.addEventListener('DOMContentLoaded', initial_setup);
}());
