// ==UserScript==
// @name          Redirect image to original
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Redirect scaled images and thumbnails to their originals
// @include       /^https?://(postfiles|[^.]*blogthumb-phinf)[^.]*\.(naver|pstatic)\.net/.*\.jpe?g/
// @include       /^https?://[^.]+\.(naver|pstatic)\.net/.*\.(jpe?g|gif|png)\?.*$/
// @exclude       /^https?://blogfiles\.naver\.net/.*\.jpe?g/
// @include       /^https?://vfan-phinf\.(naver|pstatic)\.net/.*\.jpe?g(\?type=.+)?
// @exclude       /^https?://vfan-phinf\.(naver|pstatic)\.net/.*\.jpe?g\?attachment/
// @include       /^https?://(.+\.daumcdn\.net/thumb/.*)?cfile(\d+)\.uf\.(tistory\.com|daum\.net).+([0-9A-Z]{22})$/
// @exclude       /^https?://cfile\d+\.uf\.tistory\.com/original//
// @include       /^https?://t\d+\.daumcdn\.net/cfile/tistory/[0-9A-Z]{18}$/
// @include       /^https?:\/\/img\d+\.daumcdn\.net/thumb/.*\?.*fname=https?.*\.jpg.*$/
// @include       /^https?://(lh.*\.googleusercontent\.com|.*\.bp\.blogspot\.com).*/[swh]\d+(-[a-z][a-z0-9=,-]*)?/[^/]+$/
// @exclude       /^https?://(lh.*\.googleusercontent\.com|.*\.bp\.blogspot\.com).*/s0/[^/]+$/
// @include       http*://pbs.twimg.com/media/*.jpg*
// @exclude       http*://pbs.twimg.com/media/*.jpg:orig
// @include       http*://pbs.twimg.com/media/*?*format=jpg*
// @include       /^https?://addyk\.co\.kr/wordpress/wp-content/uploads/.*-\d+x\d+\.jpg$/
// @include       /^https?://image\.news1\.kr/.*/(article|no_water)\.jpg$/
// @include       http*://*.img.topstarnews.net/*/file_attach*/*.jpg
// @exclude       http*://uhd.img.topstarnews.net/*/file_attach/*-org.jpg
// @include       http*://www.topstarnews.net/news/thumbnail/*_v*.jpg
// @include       http*://www.topstarnews.net/news/photo/*.jpg
// @exclude       http*://www.topstarnews.net/news/photo/*_org.jpg
// @include       /^https?://(img\.)?tenasia\.hankyung\.com/.*-\d+x\d+\.jpg$/
// @include       /^https?://[^/]+/PHOTOBANK/(small|mid)_image/[A-Z]\d+/.+\.[^.]+$/
// @include       /^https?://img\d+.sbs.co.kr/img/sbs/.+/[^/]+_w\d+_h\d+.jpg$/
// @include       /^https?://ojsfile\.ohmynews\.com/PHT_IMG_FILE/.+_PHT\.jpg$/
// @include       /^https?://([^/]+\.)?gifyu\.com/images/.+\.md\.[^.]+$/
// @include       /^https?://([^/]+\.)?sinaimg\.cn/mw\d+/.+$/
// @exclude       /^https?://([^/]+\.)?sinaimg\.cn/mw2048/.+$/
// @include       /^https?://file\.osen\.co\.kr/article(_thumb)?/\d.*\.jpg$/
// @include       /^https?://img\.\d+\.inews24\.com/\d+x/.*\.jpg$/
// @version       1.24
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var debug, services;

  debug = false;

  services = [
    // vlive (must come before naver)
    {
      'url_regex': new RegExp(
        'https?://vfan-phinf\\.(naver|pstatic)\\.net/.*\\.jpe?g(\\?type=.+)?$', 'i'),
      'subs': [
        [/(\?[^?]+)?$/, '?attachment']
      ],
      'inline': true,
    },
    // naver blog, naver post, naver entertainment
    {
      'url_regex': new RegExp(
        'https?://[^/]+\\.(naver|pstatic)\\.net/[^"\']*\\.(jpe?g|gif|png)', 'i'),
      'subs': [
        [/\/\/(postfiles[^.]*|[^.]*blogthumb-phinf)\./, '//blogfiles.'],
        [/\?[^?]+$/, '']
      ]
    },
    // old tistory
    {
      'url_regex': new RegExp(
        'https?://(.+\\.daumcdn\\.net/thumb/.*)?'+
          'cfile(\\d+)\\.uf\\.(tistory\\.com|daum\\.net)'+
          '.+([0-9A-Z]{22})$', 'i'),
      'subs': [
        [
          /https?:\/\/(.+\.daumcdn\.net\/thumb\/.*)?cfile(\d+)\.uf\.(tistory\.com|daum\.net).+([0-9A-Z]{22})$/i,
          'http://cfile$2.uf.$3/original/$4'
        ],
      ],
      'unquote': true
    },
    // new tistory
    {
      'url_regex': new RegExp(
        'https?://t\\d+\\.daumcdn\\.net/cfile/tistory/[0-9A-Z]{18}$', 'i'),
      'subs': [
        [/(\?.*)?$/, '?original']
      ]
    },
    // daumcdn thumbs
    {
      'url_regex': new RegExp(
        'https?://img\\d+\\.daumcdn\\.net/thumb/.*\\?.*fname=https?.*\\.jpg.*$', 'i'),
      'subs': [
        [/https?:\/\/img\d+\.daumcdn\.net\/thumb\/.*\?.*fname=/i, ''],
        [/\.jpg.+$/i, '.jpg']
      ],
      'unquote': true
    },
    // googleplus, blogspot
    {
      'url_regex': new RegExp(
        'https?://(lh.*\\.googleusercontent\\.com|.*\\.bp\\.blogspot\\.com)/'+
          '.*/[swh]\\d+(-[a-z][a-z0-9=,-]*)?/[^/]+$', 'i'),
      'subs': [
        [/\/[swh]\d+(-[a-z][a-z0-9=,-]*)?\/([^\/]+)$/, '/s0/$2']
      ]
    },
    // twitter
    {
      'url_regex': new RegExp(
        'https?://pbs\\.twimg\\.com/media/.*\\.jpg(:[a-z]+)?$', 'i'),
      'subs': [
        [/\.jpg(:[a-z]+)?$/, '.jpg:orig']
      ]
    },
    {
      'url_regex': new RegExp(
        'https?://pbs\\.twimg\\.com/media/[^./"\']+\\?[^"\']*format=jpg', 'i'),
      'subs': [
        [/\?.*/, '.jpg:orig']
      ]
    },
    // addyk
    {
      'url_regex': new RegExp(
        'https?://addyk\\.co\\.kr/wordpress/wp-content/uploads/.*-\\d+x\\d+\\.jpg$', 'i'),
      'subs': [
        [/-\d+x\d+\.jpg$/, '.jpg']
      ]
    },
    // news1
    {
      'url_regex': new RegExp(
        '^https?://image\\.news1\\.kr/.*/(article|no_water)\\.jpg$', 'i'),
      'subs': [
        [/(article|no_water)\.jpg$/, 'original.jpg'],
        [/\/\/system/, '/system']
      ]
    },
    // topstarnews
    {
      'url_regex': new RegExp(
        '^https?://(main|uhd)\\.img\\.topstarnews\\.net/.*/file_attach(_thumb)?/'+
        '.+/[0-9-]+(_.*|-org)?\\.jpg$', 'i'),
      'subs': [
        [/main\.img\.topstarnews/, 'uhd.img.topstarnews'],
        [/\/file_attach_thumb\//, '/file_attach/'],
        [/([0-9-]+)(_.*)?\.jpg$/, '$1-org.jpg']
      ]
    },
    {
      'url_regex': new RegExp(
        '^https?://www\\.topstarnews\\.net/news/thumbnail/.*_v\\d+\\.jpg$', 'i'),
      'subs': [
        [/\/thumbnail\//, '/photo/'],
        [/_v\d+\.jpg$/, '_org.jpg']
      ]
    },
    {
      'url_regex': new RegExp(
        '^https?://www\\.topstarnews\\.net/news/photo/.*\\d+\\.jpg$', 'i'),
      'subs': [
        [/\.jpg$/, '_org.jpg']
      ]
    },
    // tenasia
    {
      'url_regex': new RegExp(
        '^https?://(img\\.)?tenasia\\.hankyung\\.com/.*-\\d+x\\d+\\.jpg$', 'i'),
      'subs': [
        [/-\d+x\d+\.jpg$/, '.jpg']
      ]
    },
    // kbs photobank
    {
      'url_regex': new RegExp(
        '^https?://[^/]+/PHOTOBANK/(small|mid)_image/[A-Z]\\d+/.+\\.[^.]+$'),
      'subs': [
        [/\/(small|mid)_image\//, '/origin_image/']
      ]
    },
    // sbs program
    {
      'url_regex': new RegExp(
        '^https?://img\\d+.sbs.co.kr/img/sbs/.+/[^/]+_w\\d+_h\\d+.jpg$', 'i'),
      'subs': [
        [/_w\d+_h\d+.jpg$/, '_ori.jpg']
      ]
    },
    // oh my news
    {
      'url_regex': new RegExp(
        '^https?://ojsfile\\.ohmynews\\.com/PHT_IMG_FILE/.+_PHT.jpg$', 'i'),
      'subs': [
        [/PHT_IMG_FILE/, 'BIG_IMG_FILE'],
        [/_PHT\.jpg/, '_BIG.jpg']
      ]
    },
    // gifyu
    {
      'url_regex': new RegExp(
        '^https?://([^/]+\\.)?gifyu\\.com/images/.+\\.md\\.[^.]+$', 'i'),
      'subs': [
        [/\.md(\.[^.]+)$/, '$1']
      ]
    },
    // weibo
    {
      'url_regex': new RegExp(
        '^https?://([^/]+\\.)?sinaimg\\.cn/mw\\d+/.+$', 'i'),
      'subs': [
        [/[/]mw\d+[/]/, '/mw2048/']
      ]
    },
    // osen
    {
      'url_regex': new RegExp(
        '^https?://file\\.osen\\.co\\.kr/article(_thumb)?/\\d.*\\.jpg', 'i'),
      'subs': [
        [/[/]article(_thumb)?[/]/, '/article/original/'],
        [/_\d+x\d*\.jpg/, '.jpg']
      ]
    },
    // joynews24
    {
      'url_regex': new RegExp(
        '^https?://img\\.\\d+\\.inews24\\.com/\\d+x/.*\\.jpg', 'i'),
      'subs': [
        [/[/]\d+x[/]/, '/']
      ]
    }
  ];

  function build_original_url(url, substitutions) {
    var i, regex, replacement;

    // make all substitutions applicable
    for (i=0; i < substitutions.length; i++) {
      regex = substitutions[i][0];
      replacement = substitutions[i][1];
      url = url.replace(regex, replacement);
    }

    return url;
  }

  function get_service(url) {
    var i;

    for (i=0; i < services.length; i++) {
      if (services[i].url_regex.test(url)) {
        if (debug) {
          console.log('got match for url');
        }
        return services[i];
      }
    }

    return null;
  }

  function get_original_url(service, url) {
    var original_url;

    if (!service) { return null; }

    original_url = build_original_url(url, service.subs);
    return service.unquote ? unescape(original_url) : original_url;
  }

  function remove_image() {
    /* get rid of image so it won't load twice */
    var img = document.querySelector('img');
    if (!img) {
      console.log('failed to locate img element');
      return;
    }

    img.parentNode.removeChild(img);
  }

  function check_image() {
    /* check image and redirect when applicable */
    var service, original_url, img,
      cur_url = window.location.href;

    service = get_service(cur_url);
    original_url = get_original_url(service, cur_url);

    if (debug) {
      console.log('url = '+window.location.href);
      console.log('original = '+original_url);
    }

    // make sure we don't loop infinitely
    if (!(original_url && original_url !== window.location.href)) {
      if (debug) {
        console.log('no original url or current url is already the original');
      }
      return;
    }

    if (!debug) {
      if (service.inline) {
        // we'll aplly the the solution below only on firefox, since it's known
        // to work there, while it's also known to break chrome's image
        // visualization
        if (navigator.userAgent.indexOf('Firefox') < 0) {
          console.log('not firefox; skipping inline replacement of the image');
          return;
        }

        // replace image's src inline
        /* note that we can't replace the whole img element, otherwise the
         * browser's event listeners used for resizing the image on click would
         * be lost */
        img = document.querySelector('img');
        if (!img) {
          console.log('failed to locate img element');
          return;
        }

        /* replace the url in the location bar to indicate that we have
         * replaced the image */
        img.addEventListener('load', function() {
          history.replaceState({}, '', original_url);
        });

        img.src = original_url;
      }
      else {
        // remove image, otherwise it might start loading while the browser
        // resolves the new url and so on
        remove_image();

        // redirect
        window.location.replace(original_url);
      }
    }
  }

  // run immediately since we want to redirect as soon as possible to avoid
  // loading the image for no reason
  check_image();

}());
