// ==UserScript==
// @name          Gfycat - automatically switch quality to HD
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Automatically switch video quality to HD
// @include       https://gfycat.com/*
// @version       1.0
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  // don't run on frames
  // http://stackoverflow.com/a/4190594
  if (window.top != window.self) { return; }

  function do_click(elem) {
    /* dispatch a click on an element */
    var evt = new MouseEvent(
      'click', { bubbles: true, cancelable: true, view: window});

    elem.dispatchEvent(evt);
  }

  function setup() {
    /* if the quality setting says SD, click it */
    var button = document.querySelector('.settings-button');

    if (button && !button.classList.contains('hd')) {
      // click it
      console.log('toggling quality');
      do_click(button);
    }
  }

  function initial_setup() {
    /* keep watching the page for mutations and checking the quality button
     * every time */
    var observer = new MutationObserver(setup);
    observer.observe(document, { childList: true, subtree: true });

    setup();
  }

  document.addEventListener('DOMContentLoaded', initial_setup);
}());
