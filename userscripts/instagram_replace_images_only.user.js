// ==UserScript==
// @name          Instagram - replace images
// @namespace     https://gitlab.com/loopvid/scripts
// @description   Makes instagram images clickable
// @include       http*://*.instagram.com/*
// @include       http*://instagram.com/*
// @version       4.7
// @grant         none
// @run-at        document-start
// ==/UserScript==

(function() {
  'use strict';

  var debug = false,
    new_elements_observer = new MutationObserver(look_for_new_divs),
    new_elements_observer_init = {childList: true, subtree: true};

  function is_element_on_screen(elem) {
    // based on http://stackoverflow.com/a/7557433
    if (elem.getBoundingClientRect === undefined) { return false; }

    var rect = elem.getBoundingClientRect();
    return (
      rect.right >= 0 &&
      rect.bottom >= 0 &&
      rect.left <= (window.innerWidth || document.documentelem.clientWidth) &&
      rect.top <= (window.innerHeight || document.documentelem.clientHeight)
    );
  }

  function block_image_srcset(mutations) {
    /* Block the srcset attribute, so the browser will use src, which should
     * contain the highest resolution available */

    var image = mutations[0].target;

    /* note that we check for a 1080 width size in the srcset to avoid
     * operating on thumbs, and also that we check that the src in present in
     * srcset (which should always be the case) just to make sure we don't
     * mangle all images if instagram changes the way their js handles these
     * attributes in the future */
    if (!image || !image.src || !image.srcset ||
        (image.srcset.indexOf(' 1080w') < 0) ||
        (image.srcset.indexOf(image.src) < 0)) {
      return;
    }

    image.srcset = '';
    if (debug) { console.log('blocked srcset for image'); }
  }

  function hide_image_clickshields() {
    /* Go through all unhandled images and look for their click shields. */
    var i, image, observer,
        images = document.querySelectorAll('img:not([data-handled="true"])');

    for (i=0; i<images.length; i++) {
      image = images[i];

      // try to skip images that are not photos (e.g. avatars will be inside
      // <a> tags)
      if (!(image.parentNode && image.parentNode.tagName === 'DIV')) {
        continue;
      }

      /* If the image is off screen and the parent has other children, that
       * means it's being preloaded. The page will actually insert this image
       * as a new element once the post becomes visible, so we don't need to do
       * anything here (and if we try to, we might wrongfully hide
       * non-clickshield divs) */
      if (!is_element_on_screen(image) &&
          image.parentNode.childNodes.length > 1) {
        continue;
      }

      // if this is an image cover for a video, we don't have to do anything
      if (image.parentNode.querySelector('video')) {
        image.dataset.handled = 'true';
        return;
      }

      // if the image has a div sibling, that's the clickshield, otherwise it
      // will be its parent's sibling
      if (!image.parentNode.querySelector('div')) {
        image.parentNode.classList.add('clickshield_sibling');
      }

      // observe image attributes to block srcset
      observer = new MutationObserver(block_image_srcset);
      observer.observe(image, {'attributes': true});

      // mark image so we don't process it again in the future
      image.dataset.handled = 'true';
    }
  }

  function look_for_new_divs() {
    /* Go through all images and videos looking for new ones after a mutation */

    // hide click shield divs to expose the images underneath
    hide_image_clickshields();
  }

  function setup_css() {
    /* insert new sheet for our custom rules */
    var sheet, elem = document.createElement('style');

    document.head.appendChild(elem);

    // note that we can only access the sheet after appending the element to
    // <head>
    sheet = elem.sheet;

    /* hide clickshields */
    sheet.insertRule(
      '.clickshield_sibling+div, '+
      'img[data-handled="true"]+div { display: none; }',
      sheet.cssRules.length);

    if (debug) { console.log('custom css inserted'); }
  }

  function setup() {
    if (debug) { console.log('setting up'); }

    /* setup CSS to unblock images */
    setup_css();

    /* watch whole DOM for new elements being added */
    new_elements_observer.observe(document, new_elements_observer_init);

    if (debug) { console.log('done'); }
  }

  document.addEventListener('DOMContentLoaded', setup);

}());
